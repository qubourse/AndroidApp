package com.example.leque.projet2;

import android.hardware.Sensor;
import android.hardware.SensorManager;



public class Orientation
{
    float x, y, z;
    private SensorManager sensorManager;
    public SensorManager getSensorManager()
    {
        return sensorManager;
    }
    private Sensor magnetic;
    private Sensor accelerometer;

    public Orientation(){

    }



    public void setSensorManager(SensorManager sensorManager)
    {
        this.sensorManager = sensorManager;
    }



    public Sensor getMagnetic()
    {
        return magnetic;
    }

    public void setMagnetic(Sensor magnetic)
    {
        this.magnetic = magnetic;
    }



    public Sensor getAccelerometer()
    {
        return accelerometer;
    }

    public void setAccelerometer(Sensor accelerometer)
    {
        this.accelerometer = accelerometer;
    }
}
