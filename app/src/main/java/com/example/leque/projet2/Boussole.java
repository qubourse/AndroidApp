package com.example.leque.projet2;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import static android.R.attr.type;
import static android.content.ContentValues.TAG;


public class Boussole extends AppCompatActivity implements SensorEventListener
{
    SensorManager sensorManager;
    float[] accelerometerVector=new float[3];
    float[] magneticVector=new float[3];
    float[] resultMatrix=new float[9];
    float[] values=new float[3];
    float cap_fixe = 0;
    float cap_reel = 0;
    float écart = 0;

    Orientation orientation = new Orientation();

    //private static final int ACCELE = 0;
    //private static final int MAGNE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boussole);
        sensorManager = (SensorManager)getSystemService(SENSOR_SERVICE);
        orientation.setSensorManager(sensorManager);
        orientation.setMagnetic(sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD));
        orientation.setAccelerometer(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
    }

    private String getType(int Type)
    {
        String strType;
        switch (type)
        {
            case Sensor.TYPE_ACCELEROMETER:
                strType = "TYPE_ACCELEROMETER";
                break;
            case Sensor.TYPE_GRAVITY:
                strType = "TYPE_GRAVITY";
                break;
            case Sensor.TYPE_GYROSCOPE:
                strType = "TYPE_GYSROSCOPE";
                break;
            case Sensor.TYPE_LIGHT:
                strType = "TYPE_LIGHT";
                break;
            case Sensor.TYPE_LINEAR_ACCELERATION:
                strType = "TYPE_LINEAR_ACCELERATION";
                break;
            case Sensor.TYPE_MAGNETIC_FIELD:
                strType = "TYPE_MAGNETIC_FIELD";
                break;
            case Sensor.TYPE_PRESSURE:
                strType = "TYPE_PRESSURE";
                break;
            case Sensor.TYPE_PROXIMITY:
                strType = "TYPE_PROXIMITY";
                break;
            case Sensor.TYPE_ROTATION_VECTOR:
                strType = "TYPE_ROTATION_VECTOR";
                break;
            default:
                strType = "TYPE_UNKNOW";
                break;
        }
        return strType;
    }

    @Override
    protected void onPause()
    {
        // désenregistrer tous le monde
        sensorManager.unregisterListener(this, orientation.getAccelerometer());
        sensorManager.unregisterListener(this, orientation.getMagnetic());
        super.onPause();
    }

    @Override
    protected void onResume()
    {
        sensorManager.registerListener(this, orientation.getAccelerometer(), SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, orientation.getMagnetic(), SensorManager.SENSOR_DELAY_UI);
        super.onResume();
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy)
    {
        // Nothing to do
    }

    @Override
    public void onSensorChanged(SensorEvent event)
    {
        // Mettre à jour la valeur de l'accéléromètre et du champ magnétique
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
        {
            accelerometerVector=event.values;
        }
        else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
        {
            magneticVector=event.values;
        }

        // Demander au sensorManager la matrice de Rotation (resultMatric)
        SensorManager.getRotationMatrix(resultMatrix, null, accelerometerVector, magneticVector);

        // Demander au SensorManager le vecteur d'orientation associé (values)
        SensorManager.getOrientation(resultMatrix, values);

        cap_reel = (float) Math.toDegrees(values[0]);
        //Log.e("Valeurs orientation", "" +orientation.x + orientation.y + orientation.z);
        écart = cap_fixe - cap_reel;
        écart = cap_fixe - cap_reel;
        //Si on obient un écart supérieur à 180°, il faut qu'on récupère son opposé
        if(écart > 180){
            écart = -360 + écart;
        }
        if(écart < -180){
            écart = 360 + écart;
        }
        Log.e(TAG, "capfixe : " + cap_fixe + " capreel : " + cap_reel + " Écart : " + écart);
    }

    public void SetDirection(View view) {
        // Demander au sensorManager la matrice de Rotation (resultMatric)

        SensorManager.getRotationMatrix(resultMatrix, null, accelerometerVector, magneticVector);

        // Demander au SensorManager le vecteur d'orientation associé (values)
        SensorManager.getOrientation(resultMatrix, values);
        cap_fixe = (float) Math.toDegrees(values[0]);
        Toast.makeText(this, "" + cap_fixe, Toast.LENGTH_LONG).show();
    }

    public void Diff_OnClick(View view){
        écart = cap_fixe - cap_reel;
        //Si on obient un écart supérieur à 180°, il faut qu'on récupère son opposé
        if(écart > 180){
            écart = -360 + écart;
        }
        if(écart < -180){
            écart = 360 + écart;
        }
        Log.e(TAG, ""  + écart);
        Toast.makeText(this, "" + écart, Toast.LENGTH_LONG).show();
    }

}
